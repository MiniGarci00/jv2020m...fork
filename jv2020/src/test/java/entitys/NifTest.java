package entitys;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class NifTest {
	
	private static Nif nifTest;
	
	@BeforeAll
	private static void initializeDefault() {
		nifTest = new Nif();
	}
	
	@Test
	void setTextTest() {
		nifTest.setText("56003575R");
		assertTrue(nifTest.getText() == "56003575R");
	}


	@Test
	void NIFCloneTest( ) {
		assertSame(nifTest, nifTest.clone());
	}
	@Test 
    public void equalTest() {
        Nif nif1 = new Nif ("45341278N");
        Nif nif2 = new Nif ("68752349B");
        Nif nif3 = new Nif ("");
        assertTrue(nif1.equals(nif1));
        assertFalse(nif1.equals(nif2));
        assertFalse(nif1.equals(nif3));
    }
	@Test
    public void testSetTextNull() {
        try { 
            nifTest.setText(null);
            fail("No debe llegar aquí...");
        } 
        catch (EntitysException e) {
        }
	}
}
